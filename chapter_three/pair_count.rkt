#lang racket

(define (count-pairs-wrong x)
  (if (pair? x)
      (+ 1 (count-pairs-wrong (car x)) (count-pairs-wrong (cdr x)))
      0))

(define (list-eq-contains xs x)
  (cond [(null? xs) false]
        [(eq? x (car xs)) true]
        [else (list-eq-contains (cdr xs) x)]))

(define (test-list-eq-contains)
  (println (list-eq-contains (list 'a) 'a)) ;; true
  (define x (cons 'a 'b))
  (println (list-eq-contains (list x) x)) ;; true
  (println (list-eq-contains '() '())) ;; false
  )
(test-list-eq-contains)

(define (count-pairs x)
  (define (aux x seen)
    (if (not (pair? x))
        (cons 0 seen)
        (if (list-eq-contains seen x)
            ;; Already seen this pair.
            (cons 0 seen)
            ;; Haven't seen it before.  Add 1 to the result and put it in
            ;; `seen`.
            (let* [(new-seen (cons x seen))
                  (left-res (aux (car x) new-seen))
                  (new-seen-left (cdr left-res))
                  (right-res (aux (cdr x) new-seen-left))
                  (new-seen-right (cdr right-res))]
              (cons
               (+ 1 (car left-res) (car right-res))
               new-seen-right)))))
  ;; The first value is the actual count.
  (car (aux x '())))

(define (test-count-pairs)
  (define x (cons 'a 'b))
  (define y (cons x x))
  (define z (cons x y))
  (println (count-pairs x)) ;; 1
  (println (count-pairs y)) ;; 2
  (println (count-pairs z)) ;; 3
  )
(test-count-pairs)
